using System;
using System.Collections.Generic;
using System.Linq;
using Rifteye.Data;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Util;

namespace Rifteye.GUI;

public class PlayerMenu : GuiDialog
{
    internal string PlayerUid;
    private long _everySecondListener;
    private int _selectedPlayerName;
    private readonly IClientNetworkChannel _clientNetworkChannel;

    internal RifteyeData? RifteyeData;
    private int _currentTabIndex;
    internal RifteyeServerData? RifteyeServerData;
    private bool _updateOnceNeeded;
    private int _selectedLandClaim;
    private BlockPos? _backPosition;
    private string _playerSearchFilter;

    private GuiComposer PlayerMenuComposer
    {
        get => Composers["playerMenuDialog"];
        set => Composers["playerMenuDialog"] = value;
    }
    
    private GuiComposer PlayerListComposer
    {
        get => Composers["playerListDialog"];
        set => Composers["playerListDialog"] = value;
    }

    public bool IsOpen { get; set; }

    private string[]? LandClaims { get; set; }

    private string[] SelectedAreas { get; set; }

    private LandClaim? SelectedClaim { get; set; }
    public string[]? SelectedClaimPermittedPlayers { get; set; }

    public List<SavegameCellEntry> PlayerListCells;


    public DummyInventory HotBarInventory { get; set; }
    public DummyInventory BackpackInventory { get; set; }

    public DummyInventory MouseInventory { get; set; }

    public DummyInventory CharacterInventory { get; set; }

    public DummyInventory CraftingInventory { get; set; }


    public PlayerMenu(ICoreClientAPI capi, IClientNetworkChannel clientNetworkChannel) : base(capi)
    {
        _clientNetworkChannel = clientNetworkChannel;
        PlayerUid = string.Empty;
        SelectedAreas = new[] { string.Empty };

        HotBarInventory = new DummyInventory(capi, 12);
        BackpackInventory = new DummyInventory(capi, 4);

        CraftingInventory = new DummyInventory(capi, 10);
        CharacterInventory = new DummyInventory(capi, 15);
        MouseInventory = new DummyInventory(capi);
    }

    public override string ToggleKeyCombinationCode => "rifteyeplayermenu";

    private void SetupDialog()
    {
        var dialogBounds = ElementStdBounds.AutosizedMainDialog.WithAlignment(EnumDialogArea.CenterMiddle).WithFixedAlignmentOffset(-200,0);
        var contentBounds = ElementBounds.Fixed(5, 40, 960, 520);

        var bgBounds = ElementBounds.Fill.WithFixedPadding(GuiStyle.ElementToDialogPadding);
        bgBounds.BothSizing = ElementSizing.FitToChildren;
        bgBounds.WithChildren(contentBounds);

        var tabBoundsL = ElementBounds.Fixed(-180, 25, 180, 545);
        var tabs = new[]
        {
            new GuiTab { DataInt = 0, Name = "Info", PaddingTop = 10 },
            new GuiTab { DataInt = 1, Name = "Actions", PaddingTop = 10 },
            new GuiTab { DataInt = 2, Name = "Claims", PaddingTop = 10 }
        };

        PlayerMenuComposer = capi.Gui.CreateCompo("playerMenuDialog", dialogBounds)
            .AddShadedDialogBG(bgBounds)
            .AddDialogTitleBar("Player Menu", OnTitleBarCloseClicked)
            .AddVerticalTabs(tabs, tabBoundsL, OnTabClicked, "verticalTabs");

        switch (_currentTabIndex)
        {
            case 0:
            {
                ComposePlayerMenu(dialogBounds);
                break;
            }
            case 1:
            {
                ComposeCommands();
                break;
            }
            case 2:
            {
                ComposeClaims();
                break;
            }
            default:
            {
                PlayerMenuComposer.Compose();
                break;
            }
        }
        var dialogBoundsList =
                bgBounds.ForkBoundingParent()
                    .WithAlignment(EnumDialogArea.LeftMiddle)
                    .WithFixedAlignmentOffset((dialogBounds.renderX + dialogBounds.OuterWidth + 10) / RuntimeEnv.GUIScale, 0);
        
        var contentBoundsList = ElementBounds.Fixed(5, 40, 360, 520);

        var bgBoundsList = ElementBounds.Fill.WithFixedPadding(GuiStyle.ElementToDialogPadding);
        bgBoundsList.BothSizing = ElementSizing.FitToChildren;
        bgBoundsList.WithChildren(contentBoundsList);

        ElementBounds clippingBounds,listBounds;
        var titleBounds = ElementBounds.Fixed(EnumDialogArea.LeftTop, 0, 0, 360, 35);
        _playerSearchFilter = string.Empty;
        PlayerListComposer = capi.Gui.CreateCompo("playerListDialog", dialogBoundsList)
            .AddShadedDialogBG(bgBoundsList)
            .AddDialogTitleBar("Player List", OnTitleBarCloseClicked)
            .AddTextInput(titleBounds = titleBounds.BelowCopy(10, 6).WithFixedSize(360, 30), OnSearchChanged, null, "playerSearch")
            .AddInset(bgBoundsList = titleBounds.BelowCopy(0, 3).WithFixedSize(
                contentBoundsList.fixedWidth,
                contentBoundsList.fixedHeight -3
            ))
            .AddVerticalScrollbar(OnNewScrollbarvalue, ElementStdBounds.VerticalScrollbar(bgBoundsList), "scrollbar")
            .BeginClip(clippingBounds = bgBoundsList.ForkContainingChild(3, 3, 3, 3))
            .AddCellList(listBounds = clippingBounds.ForkContainingChild(0, 0, 0, -3).WithFixedPadding(5),
                CreateCellElem, LoadPlayerCells(), "playerscells")
            .EndClip();
        
        PlayerListComposer.Compose();
        listBounds.CalcWorldBounds();
        clippingBounds.CalcWorldBounds();

        var guiElementTextInput = PlayerListComposer.GetTextInput("playerSearch");
        guiElementTextInput.SetPlaceHolderText(Lang.Get("Search..."));
        guiElementTextInput.OnFocusLost();
        PlayerListComposer.GetScrollbar("scrollbar").SetHeights(
            (float)(clippingBounds.fixedHeight),
            (float)(listBounds.fixedHeight)
        );

        PlayerMenuComposer.GetVerticalTab("verticalTabs").SetValue(_currentTabIndex, false);
    }

    private void ComposeClaims()
    {
        var font = CairoFont.WhiteSmallishText();
        const int spacing2 = 15;
        var leftText = ElementBounds.Fixed(15, 5, 180, 40);
        var rightDropDown = ElementBounds.Fixed(220, 25, 450, 20);

        LandClaims = RifteyeData?.LandClaims?.Select(c => $"{c.Description}").ToArray() ?? new[] { string.Empty };
        SelectedClaim = LandClaims.Length > 0 ? RifteyeData?.LandClaims?.FirstOrDefault() : null;
        SelectedAreas = SelectedClaim?.Areas.Select(a => $"{a.Start}  to  {a.End}").ToArray() ?? new[] { string.Empty };
        SelectedClaimPermittedPlayers = SelectedClaim?.PermittedPlayerUids
            ?.Select(p => $"{capi.World.PlayerByUid(p.Key)?.PlayerName ?? p.Key} : {p.Value}").ToArray();
        if (SelectedClaimPermittedPlayers == null || SelectedClaimPermittedPlayers.Length == 0)
        {
            SelectedClaimPermittedPlayers = new[] { string.Empty };
        }
      
        const int spacing = -2;
        PlayerMenuComposer
            .AddStaticText("Player:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(RifteyeData?.PlayerName ?? "ERROR getting name" , font, rightDropDown = rightDropDown.BelowCopy().WithFixedHeight(30), "playername")
            .AddIf(LandClaims.Length > 0)
            .AddStaticText("Claims:", font, leftText = leftText.BelowCopy(0, spacing + 10))
            .AddDropDown(LandClaims, LandClaims, _selectedLandClaim, OnSelectionClaims,
                rightDropDown = rightDropDown.BelowCopy(0, 10).WithFixedHeight(30), "claims")
            .AddStaticText("ProtectionLevel:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(SelectedClaim?.ProtectionLevel.ToString() ?? string.Empty, font,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 3), "protection")
            .AddStaticText("Areas:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDropDown(SelectedAreas, SelectedAreas, 0, OnSelectionChangeVoid,
                rightDropDown = rightDropDown.BelowCopy(0, 10).WithFixedHeight(30), "areas")
            .AddStaticText("Permitted Players:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDropDown(SelectedClaimPermittedPlayers, SelectedClaimPermittedPlayers, 0, OnSelectionChangeVoid,
                rightDropDown = rightDropDown.BelowCopy(0, 10).WithFixedHeight(30), "permittedplayers")
            .AddStaticText("AllowUseEveryone:", font, leftText.BelowCopy(0, spacing))
            .AddDynamicText(SelectedClaim?.AllowUseEveryone.ToString() ?? string.Empty, font,
                rightDropDown.BelowCopy(0, spacing2 - 8), "allowuseeveryone")
            .EndIf();
        try
        {
            PlayerMenuComposer.Compose();
        }
        catch (Exception e)
        {
            capi.Logger.Error(e.Message, e);
        }
    }
    

    private void OnSearchChanged(string searchText)
    {
        _playerSearchFilter = searchText;
        LoadFilteredPlayerList();
        PlayerListComposer.GetCellList<SavegameCellEntry>("playerscells").ReloadCells(PlayerListCells);
    }

    public void LoadFilteredPlayerList()
    {
        if (RifteyeData?.Players == null)
        {
            PlayerListCells = new List<SavegameCellEntry>();
        }

        PlayerListCells = RifteyeData?.Players?
            .Where(p => string.IsNullOrEmpty(_playerSearchFilter) || p.Key.Contains(_playerSearchFilter, StringComparison.InvariantCultureIgnoreCase))
            .Select(p => new SavegameCellEntry() { Title = p.Value, DetailText = p.Key}).ToList() ?? new List<SavegameCellEntry>();
        UpdatePlayerSelectionIndex();
        // for (int i = 0; i < 9456; i++)
        // {
        //     savegameCellEntries.Add(new SavegameCellEntry { Title = $"player-{i}" });
        // }
    }

    private List<SavegameCellEntry> LoadPlayerCells()
    {
        LoadFilteredPlayerList();
        return PlayerListCells;
    }
    
    private void OnNewScrollbarvalue(float value)
    {
        ElementBounds bounds = PlayerListComposer.GetCellList<SavegameCellEntry>("playerscells").Bounds;
        bounds.fixedY = 0 - value;

        bounds.CalcWorldBounds();
    }

    private IGuiElementCell CreateCellElem(SavegameCellEntry cell, ElementBounds bounds)
    {
        var cellElem = new GuiElementMainMenuCell(capi, cell, bounds);
        cellElem.ShowModifyIcons = false;
        cellElem.OnMouseDownOnCellLeft = OnClickCellLeft;
        return cellElem;
    }

    private void OnClickCellLeft(int obj)
    {
        foreach (var cellEntry in PlayerListCells)
        {
            cellEntry.Selected = false;
        }

        PlayerListCells[obj].Selected = true;
        _selectedPlayerName = obj;
        PlayerUid = PlayerListCells[obj].DetailText;
        var playerName = PlayerMenuComposer.GetDynamicText("playername");
        playerName?.SetNewText(PlayerListCells[obj].Title);
        _clientNetworkChannel.SendPacket(PlayerUid);

        _updateOnceNeeded = true;
    }

    private void ComposeCommands()
    {
        var font = CairoFont.WhiteSmallishText();
        var fontBtn = CairoFont.ButtonText().WithFontSize(20);
        var leftText = ElementBounds.Fixed(15, 5, 180, 40);
        var rightDropDown = ElementBounds.Fixed(200, 25, 150, 20);

        const int spacing = -2;
        var gameModes = Enum.GetNames(typeof(EnumGameMode));
        var roleIndex = RifteyeServerData?.Roles.IndexOf(RifteyeData?.Role) ?? 0;
        PlayerMenuComposer.AddStaticText("Player:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(RifteyeData?.PlayerName ?? "ERROR getting name" , font, rightDropDown = rightDropDown.BelowCopy().WithFixedHeight(30), "playername")
            .AddStaticText("GameMode:", font, leftText = leftText.BelowCopy(0, spacing + 10))
            .AddDropDown(gameModes, gameModes, (int)(RifteyeData?.CurrentGameMode ?? EnumGameMode.Survival), OnSelectionChangeVoid,
                rightDropDown = rightDropDown.BelowCopy(0, 10).WithFixedHeight(30), "gamemodes");
        var buttonBounds = ElementBounds.Fixed(rightDropDown.fixedX + rightDropDown.fixedWidth,
            rightDropDown.fixedY, 100, 20);
        PlayerMenuComposer
            .AddButton("set", OnSetGameMode, buttonBounds = buttonBounds.BelowCopy(10, -20), fontBtn);

        PlayerMenuComposer
            .AddStaticText("Role:", font, leftText = leftText.BelowCopy())
            .AddDropDown(RifteyeServerData?.Roles, RifteyeServerData?.Roles, roleIndex, OnSelectionChangeVoid,
                rightDropDown = rightDropDown.BelowCopy(0, 10).WithFixedHeight(30), "roles");
        PlayerMenuComposer
            .AddButton("set", OnRoleSet, buttonBounds = buttonBounds.BelowCopy(0, 20), fontBtn);

        PlayerMenuComposer
            .AddStaticText("Extra LCA:", font, leftText = leftText.BelowCopy())
            .AddHoverText(
                "player specific extra land claim allowance, independent of the allowance set by the role. (default: 0)",
                font, 600, leftText)
            .AddNumberInput(rightDropDown = rightDropDown.BelowCopy(0, 10).WithFixedHeight(30), OnTextChanged,
                key: "landclaimallowance");
        PlayerMenuComposer
            .AddButton("set", OnSetLandClaimAllowance, buttonBounds = buttonBounds.BelowCopy(0, 20), fontBtn);

        PlayerMenuComposer
            .AddStaticText("Extra LCMA:", font, leftText = leftText.BelowCopy())
            .AddHoverText(
                "player specific extra land claim max areas, independent of the max areas set by the role. (default: 0)",
                font, 600, leftText)
            .AddNumberInput(rightDropDown.BelowCopy(0, 10).WithFixedHeight(30), OnTextChanged,
                key: "landclaimmaxareas");
        PlayerMenuComposer
            .AddButton("set", OnSetLandClaimMaxAreas, buttonBounds.BelowCopy(0, 20), fontBtn);


        leftText = leftText.BelowCopy(0, 30);

        var buttonBounds2 = ElementBounds.Fixed(15, leftText.fixedY, 100, 20);
        var buttonBounds3 = ElementBounds.Fixed(15, leftText.fixedY + 40, 100, 20);
        PlayerMenuComposer
            .AddButton("Ban", OnBanClick, buttonBounds2.FlatCopy(), fontBtn)
            .AddButton("Kick", OnKickClick, buttonBounds2 = buttonBounds2.RightCopy(10), fontBtn)
            .AddButton("TP2P", OnTpToPlayerClick, buttonBounds2 = buttonBounds2.RightCopy(20), fontBtn)
            .AddHoverText("Teleport to selected player", font, 300,
                buttonBounds2)
            .AddButton("TPP2ME", OnTpPlayerToMeClick, buttonBounds2 = buttonBounds2.RightCopy(10), fontBtn)
            .AddHoverText("Teleport selected player to me", font, 300,
                buttonBounds2)
            .AddButton("ACS1", OnCharSelClick, buttonBounds3 = buttonBounds3.FlatCopy(), fontBtn)
            .AddHoverText("Allows the player to re-select their class after doing so already.", font, 300,
                buttonBounds3)
            ;

        var leftText2 = ElementBounds.Fixed(15, buttonBounds3.fixedY + 20, 105, 40);
        var rightDropDown2 = ElementBounds.Fixed(120, leftText2.fixedY + 10, 220, 20);
        PlayerMenuComposer
            .AddStaticText("TP Back:", font, leftText2 = leftText2.BelowCopy())
            .AddHoverText("When first opened this position is saved so you can TP here. Use set to update to current position.",font , 300, leftText2)
            .AddDynamicText(_backPosition?.ToString() ?? "-", font, rightDropDown2 = rightDropDown2.BelowCopy(0, 10).WithFixedHeight(30),
                key: "tpbackpos");
        var buttonBounds4 = ElementBounds.Fixed(buttonBounds.fixedX - buttonBounds.fixedWidth, rightDropDown2.fixedY, 100, 20);
        PlayerMenuComposer
            .AddButton("set", OnTpBackSet, buttonBounds4 = buttonBounds4.RightCopy(), fontBtn)
            .AddButton("TP Back", OnTpBack, buttonBounds4.RightCopy(10), fontBtn);

        PlayerMenuComposer.Compose();

        PlayerMenuComposer.GetNumberInput("landclaimallowance").SetValue(RifteyeData?.ExtraLandClaimAllowance ?? 0);
        PlayerMenuComposer.GetNumberInput("landclaimmaxareas").SetValue(RifteyeData?.ExtraLandClaimAreas ?? 0);
    }

    private bool OnTpBackSet()
    {
        _backPosition = capi.World.Player.Entity.Pos.AsBlockPos;
        PlayerMenuComposer.GetDynamicText("tpbackpos").SetNewText(_backPosition.ToString());
        return true;
    }

    private bool OnTpBack()
    {
        if (_backPosition != null)
            capi.SendChatMessage($"/tp ={_backPosition.X} {_backPosition.Y} ={_backPosition.Z}");
        return true;
    }

    private bool OnCharSelClick()
    {
        capi.SendChatMessage($"/player {RifteyeData?.PlayerName} allowcharselonce");
        return true;
    }

    private bool OnRoleSet()
    {
        var role = PlayerMenuComposer.GetDropDown("roles").SelectedValue.ToLower();
        capi.SendChatMessage($"/player {RifteyeData?.PlayerName} role {role}");
        return true;
    }

    private bool OnSetLandClaimAllowance()
    {
        var num = (int)PlayerMenuComposer.GetNumberInput("landclaimallowance").GetValue();
        capi.SendChatMessage($"/player {RifteyeData?.PlayerName} landclaimallowance {num}");
        return true;
    }

    private bool OnSetLandClaimMaxAreas()
    {
        var num = (int)PlayerMenuComposer.GetNumberInput("landclaimmaxareas").GetValue();
        capi.SendChatMessage($"/player {RifteyeData?.PlayerName} landclaimmaxareas {num}");
        return true;
    }

    private static void OnTextChanged(string obj)
    {
    }

    private bool OnSetGameMode()
    {
        var gameMode = PlayerMenuComposer.GetDropDown("gamemodes").SelectedValue.ToLower();

        capi.SendChatMessage($"/gamemode {RifteyeData?.PlayerName} {gameMode}");
        return true;
    }

    private void ComposePlayerMenu(ElementBounds dialogBounds)
    {
        var leftText = ElementBounds.Fixed(15, 5, 150, 40);
        var rightDropDown = ElementBounds.Fixed(170, 25, 300, 20);

        var offhandBounds = ElementStdBounds.SlotGrid(EnumDialogArea.LeftFixed, 20, 0, 10, 1);
        var hotBarBounds = ElementStdBounds.SlotGrid(EnumDialogArea.LeftFixed, 80, 0, 10, 1);
        var backpackBounds = ElementStdBounds.SlotGrid(EnumDialogArea.RightFixed, -180, 0, 4, 1)
            .WithFixedAlignmentOffset(-10, 0);

        var rows = BackpackInventory.Slots != null ? (int)Math.Ceiling(BackpackInventory.Slots.Length / 6f) : 0;

        const double pad = 3.0;
        var craftingGridBounds = ElementStdBounds.SlotGrid(EnumDialogArea.RightFixed, -292, 50, 3, 3);

        const int spacing = -2;
        const int spacing2 = 15;

        var font = CairoFont.WhiteSmallishText();

        PlayerMenuComposer.AddStaticText("Player:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(RifteyeData?.PlayerName ?? "ERROR getting name" , font, rightDropDown = rightDropDown.BelowCopy().WithFixedHeight(30), "playername")
            .AddStaticText("GM / FM / NC:", font, leftText = leftText.BelowCopy(0, spacing), "gmt")
            ;
        PlayerMenuComposer.AddDynamicText(
                $"{RifteyeData?.CurrentGameMode} / {RifteyeData?.FreeMove} / {RifteyeData?.NoClip}", font,
                rightDropDown = rightDropDown.BelowCopy(), "gm")
            ;
        PlayerMenuComposer.AddHoverText("Gamemod / FreeMove / NoClip", font, 300, leftText)
            ;
        PlayerMenuComposer.AddStaticText("Privileges:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDropDown(RifteyeData?.Privileges, RifteyeData?.Privileges, 0,
                OnSelectionChangeVoid,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 10), "privileges")
            .AddStaticText("PlayerUID:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(RifteyeData?.PlayerUid, font,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 3), "uid")
            .AddStaticText("Health:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText($"{RifteyeData?.Health} / {RifteyeData?.MaxHealth}", font,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 8), "health")
            .AddStaticText("Hunger:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText($"{RifteyeData?.Saturation} / {RifteyeData?.MaxSaturation}", font,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 5), "hunger")
            .AddStaticText("Pos / MSpeed:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(RifteyeData?.Position + " / " + RifteyeData?.MoveSpeedMultiplier, font,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 8).WithFixedWidth(600), "pos")
            .AddHoverText("Position / MoveSpeed", font, 300, leftText)
            .AddStaticText("Class:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(RifteyeData?.Class, font,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 8).WithFixedWidth(600), "class")
            .AddStaticText("Role:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddDynamicText(RifteyeData?.Role, font,
                rightDropDown = rightDropDown.BelowCopy(0, spacing2 - 8).WithFixedWidth(600), "role")
            .AddStaticText("Respawn:", font, leftText = leftText.BelowCopy(0, spacing))
            .AddHoverText("Respawn uses left if set (99 usually means none is set)", font, 300, leftText)
            .AddDynamicText(RifteyeData?.RespawnUses.ToString(), font,
                rightDropDown.BelowCopy(0, spacing2 - 8).WithFixedWidth(600), "respawn")
            .AddStaticText("Inventory:", font, leftText = leftText.BelowCopy(0, spacing));

        leftText = leftText.BelowCopy();

        var y = leftText.BelowCopy().fixedY - 40;
        offhandBounds.fixedY = y;
        hotBarBounds.fixedY = y;
        backpackBounds.fixedY = y;
        const int posSlot = 470;

        var slotGridBounds = ElementStdBounds.SlotGrid(EnumDialogArea.None, posSlot + pad, 210 + pad, 6, 4)
            .FixedGrow(2 * pad, 2 * pad);
        var fullGridBounds = ElementStdBounds.SlotGrid(EnumDialogArea.None, 0, 0, 6, rows);

        var creativeClippingBounds = slotGridBounds.ForkBoundingParent();
        var insetBounds = creativeClippingBounds.ForkBoundingParent(6, 3, 0, 3);
        var scrollbarBounds = ElementStdBounds.VerticalScrollbar(insetBounds).WithParent(dialogBounds);

        scrollbarBounds.fixedOffsetX -= 2;
        scrollbarBounds.fixedWidth = 15;


        PlayerMenuComposer.AddItemSlotGrid(HotBarInventory, SendInvPacket, 1, new[] { 11 }, offhandBounds,
                "offhandgrid")
            .AddItemSlotGrid(HotBarInventory, SendInvPacket, 10, Enumerable.Range(0, Math.Min(HotBarInventory.Count, 10)).ToArray(),
                hotBarBounds, "hotbargrid")
            .AddItemSlotGrid(BackpackInventory, SendInvPacket, 4, Enumerable.Range(0, Math.Min(BackpackInventory.Count, 4)).ToArray(), backpackBounds,
                "backpackgrid")
            .AddInset(insetBounds)
            .BeginClip(creativeClippingBounds)
            .AddItemSlotGridExcl(BackpackInventory, SendInvPacket, 6, Enumerable.Range(0, Math.Min(BackpackInventory.Count, 4)).ToArray(), fullGridBounds,
                "slotgrid")
            .EndClip()
            .AddVerticalScrollbar(OnNewScrollbarValue, scrollbarBounds, "scrollbar");

        PlayerMenuComposer.AddItemSlotGrid(CraftingInventory, SendInvPacket, 3, Enumerable.Range(0, Math.Min(CraftingInventory.Count, 9)).ToArray(),
                craftingGridBounds.FlatCopy(),
                "craftinggrid")
            .AddItemSlotGrid(CraftingInventory, SendInvPacket, 1, new[] { 9 },
                craftingGridBounds = craftingGridBounds.RightCopy(0, 50), "outputslot");

        PlayerMenuComposer.AddItemSlotGrid(MouseInventory, SendInvPacket, 1, new[] { 0 },
            craftingGridBounds.RightCopy(-40), "mouseslot");

        PlayerMenuComposer.GetSlotGrid("offhandgrid").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGrid("hotbargrid").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGrid("backpackgrid").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGridExcl("slotgrid").CanClickSlot = CanClickSlot;

        PlayerMenuComposer.GetSlotGrid("craftinggrid").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGrid("outputslot").CanClickSlot = CanClickSlot;

        PlayerMenuComposer.GetSlotGrid("mouseslot").CanClickSlot = CanClickSlot;


        const int cx = 820;
        const int cy = 170;
        var leftArmorSlotBoundsHead =
            ElementStdBounds.SlotGrid(EnumDialogArea.None, cx, cy + pad, 1, 1).FixedGrow(0, pad);
        var leftSlotBounds = ElementStdBounds.SlotGrid(EnumDialogArea.None, cx, cy + pad, 1, 6).FixedGrow(0, pad);
        leftSlotBounds.FixedRightOf(leftArmorSlotBoundsHead, 10);

        var leftArmorSlotBoundsBody = ElementStdBounds.SlotGrid(EnumDialogArea.None, cx, cy + pad + 102, 1, 1)
            .FixedGrow(0, pad);
        leftSlotBounds.FixedRightOf(leftArmorSlotBoundsBody, 10);

        var leftArmorSlotBoundsLegs = ElementStdBounds.SlotGrid(EnumDialogArea.None, cx, cy + pad + 204, 1, 1)
            .FixedGrow(0, pad);
        leftSlotBounds.FixedRightOf(leftArmorSlotBoundsLegs, 10);

        var rightSlotBounds =
            ElementStdBounds.SlotGrid(EnumDialogArea.None, cx, cy + pad, 1, 6).FixedGrow(0, pad);
        rightSlotBounds.FixedRightOf(leftSlotBounds, 10);

        leftSlotBounds.fixedHeight -= 6;
        rightSlotBounds.fixedHeight -= 6;
        PlayerMenuComposer
            .AddItemSlotGrid(CharacterInventory, SendInvPacket, 1, new[] { 12 }, leftArmorSlotBoundsHead,
                "armorSlotsHead")
            .AddItemSlotGrid(CharacterInventory, SendInvPacket, 1, new[] { 13 }, leftArmorSlotBoundsBody,
                "armorSlotsBody")
            .AddItemSlotGrid(CharacterInventory, SendInvPacket, 1, new[] { 14 }, leftArmorSlotBoundsLegs,
                "armorSlotsLegs")
            .AddItemSlotGrid(CharacterInventory, SendInvPacket, 1, new[] { 0, 1, 2, 11, 3, 4 }, leftSlotBounds,
                "leftSlots")
            .AddItemSlotGrid(CharacterInventory, SendInvPacket, 1, new[] { 6, 7, 8, 10, 5, 9 }, rightSlotBounds,
                "rightSlots");

        PlayerMenuComposer.Compose();


        PlayerMenuComposer.GetScrollbar("scrollbar").SetHeights(
            (float)slotGridBounds.fixedHeight,
            (float)(fullGridBounds.fixedHeight + pad)
        );

        PlayerMenuComposer.GetSlotGrid("armorSlotsHead").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGrid("armorSlotsBody").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGrid("armorSlotsLegs").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGrid("leftSlots").CanClickSlot = CanClickSlot;
        PlayerMenuComposer.GetSlotGrid("rightSlots").CanClickSlot = CanClickSlot;
    }

    private void OnTabClicked(int arg1, GuiTab tab)
    {
        _currentTabIndex = tab.DataInt;
        PlayerMenuComposer.GetVerticalTab("verticalTabs").SetValue(_currentTabIndex, false);
        SetupDialog();
    }

    private void OnNewScrollbarValue(float value)
    {
        if (!IsOpened()) return;
        var bounds = PlayerMenuComposer.GetSlotGridExcl("slotgrid").Bounds;
        bounds.fixedY = 10 - GuiElementItemSlotGridBase.unscaledSlotPadding - value;

        bounds.CalcWorldBounds();
    }

    private bool OnKickClick()
    {
        if (RifteyeData?.PlayerUid?.Equals(capi.World.Player.PlayerUID) == true)
        {
            capi.ShowChatMessage("Don't kick yourself :P");
            return true;
        }

        capi.SendChatMessage($"/kick {RifteyeData?.PlayerName} by admin");
        PlayerUid = capi.World.Player.PlayerUID;
        UpdatePlayerSelectionIndex();
        _updateOnceNeeded = true;
        return true;
    }

    private bool OnBanClick()
    {
        if (RifteyeData?.PlayerUid?.Equals(capi.World.Player.PlayerUID) == true)
        {
            capi.ShowChatMessage("Don't ban yourself :P");
            return true;
        }

        capi.SendChatMessage($"/ban {RifteyeData?.PlayerName} 50 year by admin");
        PlayerUid = capi.World.Player.PlayerUID;
        UpdatePlayerSelectionIndex();
        _updateOnceNeeded = true;
        return true;
    }

    private void UpdatePlayerSelectionIndex()
    {
        for (var i = 0; i < RifteyeData?.Players?.Count; i++)
        {
            if (RifteyeData?.Players?.Keys.ElementAt(i).Equals(PlayerUid) != true) continue;

            _selectedPlayerName = i;
            foreach (var cellEntry in PlayerListCells)
            {
                cellEntry.Selected = cellEntry.DetailText.Equals(PlayerUid);
            }
            var playerName = PlayerMenuComposer.GetDynamicText("playername");
            if(playerName != null)
            {
                playerName.SetNewText(RifteyeData?.PlayerName ?? "ERROR get name");
                playerName.RecomposeText();
            }
            break;
        }

    }

    private static bool CanClickSlot(int slotId)
    {
        return false;
    }

    private bool OnTpToPlayerClick()
    {
        capi.SendChatMessage($"/tp {capi.World.Player.PlayerName} {RifteyeData?.PlayerName}");
        return true;
    }

    private bool OnTpPlayerToMeClick()
    {
        capi.SendChatMessage($"/tp {RifteyeData?.PlayerName} {capi.World.Player.PlayerName}");
        return true;
    }

    private static void SendInvPacket(object obj)
    {
    }

    private static void OnSelectionChangeVoid(string code, bool selected)
    {
    }

    private void OnSelectionClaims(string code, bool selected)
    {
        // protection
        //     areas
        // permittedplayers
        LandClaims = RifteyeData?.LandClaims?.Select(c => $"{c.Description}").ToArray() ?? new[] { string.Empty };
        var index = 0;
        for (var i = 0; i < LandClaims?.Length; i++)
        {
            if (!LandClaims[i].Equals(code)) continue;

            _selectedLandClaim = index = i;
            break;
        }

        SelectedClaim = RifteyeData?.LandClaims?.ElementAt(index);
        UpdateClaimsMenu();
    }

    private void UpdateClaimsMenu()
    {
        PlayerMenuComposer.GetDropDown("claims")?.SetList(LandClaims, LandClaims);
        PlayerMenuComposer.GetDropDown("claims")?.SetSelectedIndex(_selectedLandClaim);

        SelectedAreas = SelectedClaim?.Areas?.Select(a => $"{a.Start}  to  {a.End}").ToArray() ?? new[] { string.Empty };
        SelectedClaimPermittedPlayers = SelectedClaim?.PermittedPlayerUids
            .Select(p => $"{capi.World.PlayerByUid(p.Key)?.PlayerName ?? p.Key} : {p.Value}").ToArray();

        if (SelectedClaimPermittedPlayers == null || SelectedClaimPermittedPlayers.Length == 0)
        {
            SelectedClaimPermittedPlayers = new[] { string.Empty };
        }

        if (SelectedClaim != null)
        {
            PlayerMenuComposer.GetDynamicText("protection")?.SetNewText(SelectedClaim.ProtectionLevel.ToString());
            PlayerMenuComposer.GetDynamicText("areasnum")?.SetNewText(SelectedClaim?.Areas?.Count.ToString());
        }

        PlayerMenuComposer.GetDropDown("permittedplayers")
            ?.SetList(SelectedClaimPermittedPlayers, SelectedClaimPermittedPlayers);
        PlayerMenuComposer.GetDropDown("permittedplayers")?.SetSelectedIndex(0);
        PlayerMenuComposer.GetDropDown("areas")?.SetList(SelectedAreas, SelectedAreas);
        PlayerMenuComposer.GetDropDown("areas")?.SetSelectedIndex(0);
    }

    public override bool TryOpen()
    {
        if (RifteyeData?.Players?.Count > _selectedPlayerName)
        {
            PlayerUid = RifteyeData?.Players.ElementAt(_selectedPlayerName).Key ?? capi.World.Player.PlayerUID;
        }
        else
        {
            PlayerUid = RifteyeData?.Players?.First().Key ?? capi.World.Player.PlayerUID;
            _selectedPlayerName = 0;
        }

        _everySecondListener = capi.Event.RegisterGameTickListener(OnEvery1s, 1000);

        _backPosition ??= capi.World.Player?.Entity?.Pos?.AsBlockPos ?? new BlockPos(0);

        SetupDialog();
        return base.TryOpen();
    }

    private void OnEvery1s(float obj)
    {
        if (!IsOpened()) return;

        UpdatePlayerData();
    }

    private void UpdateCommands()
    {
        if (RifteyeData == null) return;
        PlayerMenuComposer.GetNumberInput("landclaimallowance")?.SetValue(RifteyeData.ExtraLandClaimAllowance);
        PlayerMenuComposer.GetNumberInput("landclaimmaxareas")?.SetValue(RifteyeData.ExtraLandClaimAreas);

        PlayerMenuComposer.GetDropDown("gamemodes")?.SetSelectedIndex((int)RifteyeData.CurrentGameMode);
        PlayerMenuComposer.GetDropDown("roles")?.SetSelectedIndex(RifteyeServerData?.Roles.IndexOf(RifteyeData.Role) ?? 0);
    }

    private void UpdatePlayerData()
    {
        var guiPlayerNames = PlayerMenuComposer.GetDropDown("playernames");
        guiPlayerNames?.SetList(RifteyeData?.Players?.Keys.ToArray(), RifteyeData?.Players?.Values.ToArray());

        _clientNetworkChannel.SendPacket(PlayerUid);
    }

    private void UpdatePlayerMenu()
    {
        if (RifteyeData == null) return;

        var priv = PlayerMenuComposer.GetDropDown("privileges");
        priv?.SetList(RifteyeData.Privileges, RifteyeData.Privileges);
        if (priv?.SelectedIndices[0] > RifteyeData?.Privileges?.Length)
        {
            priv.SetSelectedIndex(0);
        }

        var gm = PlayerMenuComposer.GetDynamicText("gm");
        gm?.SetNewTextAsync(
            $"{RifteyeData?.CurrentGameMode} / {RifteyeData?.FreeMove} / {RifteyeData?.NoClip}");

        var pos = PlayerMenuComposer.GetDynamicText("pos");
        if (capi.World.Config.GetBool("allowCoordinateHud"))
        {
            pos?.SetNewTextAsync(RifteyeData?.Position + " / " + RifteyeData?.MoveSpeedMultiplier);
        }
        else
        {
            pos?.SetNewTextAsync("Disabled / " + RifteyeData?.MoveSpeedMultiplier);
        }

        var playerClass = PlayerMenuComposer.GetDynamicText("class");
        playerClass?.SetNewTextAsync(RifteyeData?.Class);

        var role = PlayerMenuComposer.GetDynamicText("role");
        role?.SetNewTextAsync(RifteyeData?.Role);

        var respawn = PlayerMenuComposer.GetDynamicText("respawn");
        respawn?.SetNewTextAsync(RifteyeData?.RespawnUses.ToString());

        var uid = PlayerMenuComposer.GetDynamicText("uid");
        uid.SetNewTextAsync(RifteyeData?.PlayerUid);

        var healthText = PlayerMenuComposer.GetDynamicText("health");
        healthText?.SetNewTextAsync($"{RifteyeData?.Health} / {RifteyeData?.MaxHealth}");
        var hunger = PlayerMenuComposer.GetDynamicText("hunger");
        hunger?.SetNewTextAsync($"{RifteyeData?.Saturation} / {RifteyeData?.MaxSaturation}");
        if (RifteyeData?.RifteyeInventoryData?.Backpack?.Length != BackpackInventory.Count)
        {
            BackpackInventory = new DummyInventory(capi, RifteyeData?.RifteyeInventoryData?.Backpack?.Length ?? 0);
            SetupDialog();
        }

        for (var i = 0; i < RifteyeData?.RifteyeInventoryData?.HotBar?.Length; i++)
        {
            UpdateSlot(HotBarInventory[i], RifteyeData.RifteyeInventoryData.HotBar[i]);
        }

        for (var i = 0; i < RifteyeData?.RifteyeInventoryData?.Backpack?.Length; i++)
        {
            UpdateSlot(BackpackInventory[i], RifteyeData.RifteyeInventoryData.Backpack[i]);
        }

        for (var i = 0; i < RifteyeData?.RifteyeInventoryData?.Character?.Length; i++)
        {
            // do not update slots that we do not know about (other mods may add more slots)
            if (i < CharacterInventory.Count)
            {
                UpdateSlot(CharacterInventory[i], RifteyeData.RifteyeInventoryData.Character[i]);
            }
        }

        for (var i = 0; i < RifteyeData?.RifteyeInventoryData?.Crafting?.Length; i++)
        {
            UpdateSlot(CraftingInventory[i], RifteyeData.RifteyeInventoryData.Crafting[i]);
        }

        if (RifteyeData?.RifteyeInventoryData?.Mouse?[0] != null)
            UpdateSlot(MouseInventory[0], RifteyeData.RifteyeInventoryData.Mouse[0]);
    }

    public override bool TryClose()
    {
        IsOpen = false;
        capi.Event.UnregisterGameTickListener(_everySecondListener);
        return base.TryClose();
    }

    private void OnTitleBarCloseClicked()
    {
        TryClose();
    }

    internal void OnReceiveData()
    {
        if (!IsOpened()) return;

        if(PlayerListCells.Count != RifteyeData?.Players?.Count)
            LoadFilteredPlayerList();

        switch (_currentTabIndex)
        {
            case 0:
            {
                UpdatePlayerMenu();
                break;
            }
            case 1:
            {
                if (_updateOnceNeeded)
                {
                    _updateOnceNeeded = false;
                    UpdateCommands();
                }

                break;
            }
            case 2:
            {
                if (_updateOnceNeeded)
                {
                    _updateOnceNeeded = false;
                    LandClaims = RifteyeData?.LandClaims?.Select(c => $"{c.Description}").ToArray() ?? new[] { string.Empty };
                    SelectedClaim = RifteyeData?.LandClaims?.FirstOrDefault();
                    _selectedLandClaim = 0;
                    UpdateClaimsMenu();
                }

                break;
            }
        }
    }

    public void UpdateSlot(ItemSlot itemSlot, RifteyeItemStackData rifteyeItemStackData)
    {
        var newStack = Rifteye.FromPacket(rifteyeItemStackData, capi.World);
        var didUpdate = newStack == null != (itemSlot.Itemstack == null) ||
                        (newStack != null && !newStack.Equals(capi.World, itemSlot.Itemstack,
                            GlobalConstants.IgnoredStackAttributes));
        itemSlot.Itemstack = newStack;
        if (didUpdate)
        {
            itemSlot.MarkDirty();
        }
    }
}
