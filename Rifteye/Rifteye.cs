﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Rifteye.Data;
using Rifteye.GUI;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;
using Vintagestory.Common;
using Vintagestory.Server;

[assembly: ModInfo("Rifteye",
    Description = "Th3Dilli rifteye",
    Website = "https://gitlab.com/Th3Dilli/",
    Authors = new[] { "Th3Dilli" },
    Version = "0.2.1")]

namespace Rifteye;

public class Rifteye : ModSystem
{
    private PlayerMenu _dialog = null!;
    private ICoreClientAPI _capi = null!;
    private ICoreServerAPI _sapi = null!;

    private IClientNetworkChannel _clientNetworkChannel = null!;
    private IServerNetworkChannel _serverNetworkChannel = null!;
    private readonly List<string> _receivedServerData = new();
    
    private Dictionary<string,ServerPlayer> _offlinePlayerData = new Dictionary<string,ServerPlayer>();
    private RifteyeInventoryData _emptyInventory = null!;
    private BlockPos _nullBlockPos = null!;

    public override void StartClientSide(ICoreClientAPI api)
    {
        _clientNetworkChannel = api.Network.RegisterChannel("rifteye");
        _clientNetworkChannel.RegisterMessageType(typeof(string));
        _clientNetworkChannel.RegisterMessageType(typeof(RifteyeData));
        _clientNetworkChannel.RegisterMessageType(typeof(RifteyeServerData));
        _clientNetworkChannel.SetMessageHandler<RifteyeData>(OnReceiveData);
        _clientNetworkChannel.SetMessageHandler<RifteyeServerData>(OnReceiveServerData);

        _dialog = new PlayerMenu(api, _clientNetworkChannel);

        _capi = api;
        _capi.Input.RegisterHotKey("rifteyeplayermenu", "Rifteye", GlKeys.P,
            HotkeyType.GUIOrOtherControls);
        _capi.Input.SetHotKeyHandler("rifteyeplayermenu", ToggleGui);
    }

    private void OnReceiveServerData(RifteyeServerData data)
    {
        _dialog.RifteyeServerData = data;
    }

    private void OnReceiveData(RifteyeData data)
    {
        _dialog.RifteyeData = data;
        if (!_dialog.IsOpened() && _dialog.IsOpen)
        {
            _dialog.TryOpen();
        }
        _dialog.OnReceiveData();
    }

    private bool ToggleGui(KeyCombination comb)
    {
        if (_dialog.IsOpened())
        {
            _dialog.TryClose();
        }
        else
        {
            _dialog.IsOpen = true;
            if (string.IsNullOrEmpty(_dialog.PlayerUid))
            {   
                _dialog.PlayerUid = _capi.World.Player.PlayerUID;
            }
            _clientNetworkChannel.SendPacket(_dialog.PlayerUid);
        }

        return true;
    }

    public override void StartServerSide(ICoreServerAPI api)
    {
        _sapi = api;
        _serverNetworkChannel = api.Network.RegisterChannel("rifteye");
        _serverNetworkChannel.RegisterMessageType(typeof(string));
        _serverNetworkChannel.RegisterMessageType(typeof(RifteyeData));
        _serverNetworkChannel.RegisterMessageType(typeof(RifteyeServerData));
        _serverNetworkChannel.SetMessageHandler<string>(OnRequestPlayerdata);
        _sapi.Event.PlayerDisconnect += OnPlayerDisconnect;
        
        _emptyInventory = new RifteyeInventoryData
        {
            HotBar = ToPacket(new DummyInventory(_sapi, 12)),
            Backpack = ToPacket(new DummyInventory(_sapi, 4)),
            Crafting = ToPacket(new DummyInventory(_sapi, 10)),
            Character = ToPacket(new DummyInventory(_sapi, 15)),
            Mouse = ToPacket(new DummyInventory(_sapi, 1)),
        };
        _nullBlockPos = new BlockPos(1);
    }

    private void OnPlayerDisconnect(IServerPlayer byplayer)
    {
        if (_receivedServerData.Remove(byplayer.PlayerUID) && _receivedServerData.Count == 0 && _offlinePlayerData.Count > 0)
        {
            _offlinePlayerData.Clear();
        }
    }

    private void OnRequestPlayerdata(IServerPlayer fromPlayer, string playerUid)
    {
        if (!fromPlayer.HasPrivilege(Privilege.commandplayer))
            return;

        var player = (IServerPlayer?)_sapi.World.PlayerByUid(playerUid);

        // load offline playerdata
        player ??= LoadOfflinePlayer(playerUid);
        if (player == null)
        {
            SendEmptyPlayerData(fromPlayer, playerUid);
            return;
        }

        float health = 15, maxHealth = 15, saturation = 1500, maxSaturation = 1500;
        var healthTree = player.Entity.WatchedAttributes.GetTreeAttribute("health");
        if (healthTree != null)
        {
            health = healthTree.TryGetFloat("currenthealth") ?? 15;
            maxHealth = healthTree.TryGetFloat("maxhealth") ?? 15;
        }

        var hungerTree = player.Entity.WatchedAttributes.GetTreeAttribute("hunger");
        if (hungerTree != null)
        {
            saturation = hungerTree.TryGetFloat("currentsaturation") ?? 1500;
            maxSaturation = hungerTree.TryGetFloat("maxsaturation") ?? 1500;
        }

        var rifteyeInventoryData = new RifteyeInventoryData
        {
            HotBar = ToPacket(player.InventoryManager.GetHotbarInventory()),
            Backpack = ToPacket(player.InventoryManager.GetOwnInventory(GlobalConstants.backpackInvClassName)),
            Crafting = ToPacket(player.InventoryManager.GetOwnInventory(GlobalConstants.craftingInvClassName)),
            Character = ToPacket(player.InventoryManager.GetOwnInventory(GlobalConstants.characterInvClassName)),
            Mouse = ToPacket(player.InventoryManager.GetOwnInventory(GlobalConstants.mousecursorInvClassName))
        };

        var players = GetPlayers();

        var data = new RifteyeData
        {
            RifteyeInventoryData = rifteyeInventoryData,
            Health = health,
            MaxHealth = maxHealth,
            Saturation = saturation,
            MaxSaturation = maxSaturation,
            CurrentGameMode = ((ServerWorldPlayerData)player.WorldData).GameMode,
            FreeMove = player.WorldData.FreeMove,
            NoClip = player.WorldData.NoClip,
            PlayerUid = player.PlayerUID,
            PlayerName = player.PlayerName ?? _sapi.PlayerData.PlayerDataByUid[playerUid].LastKnownPlayername,
            Position = player.Entity.Pos.AsBlockPos,
            MoveSpeedMultiplier = player.WorldData.MoveSpeedMultiplier,
            Privileges = player.Privileges,
            Class = player.Entity.WatchedAttributes.GetString("characterClass"),
            RespawnUses = player.GetSpawnPosition(false).UsesLeft,
            ExtraLandClaimAllowance = player.ServerData.ExtraLandClaimAllowance,
            ExtraLandClaimAreas = player.ServerData.ExtraLandClaimAreas,
            Role = player.ServerData.RoleCode,
            LandClaims = _sapi.WorldManager.SaveGame.LandClaims.Where(l => l.OwnedByPlayerUid != null && l.OwnedByPlayerUid.Equals(playerUid)).ToList(),
            Players = players
        };

        _serverNetworkChannel.SendPacket(data, fromPlayer);

        if (_receivedServerData.Contains(fromPlayer.PlayerUID)) return;
        
        _receivedServerData.Add(fromPlayer.PlayerUID);
        var rifteyeServerData = new RifteyeServerData
        {
            Roles = _sapi.Server.Config.Roles.Select(r => r.Code).ToArray()
        };
        _serverNetworkChannel.SendPacket(rifteyeServerData, fromPlayer);
    }

    private void SendEmptyPlayerData(IServerPlayer fromPlayer, string playerUid)
    {
        var players = GetPlayers();
        _sapi.PlayerData.PlayerDataByUid.TryGetValue(playerUid, out var player);
        
        var data = new RifteyeData
        {
            RifteyeInventoryData = _emptyInventory,
            Health = -1,
            MaxHealth = -1,
            Saturation = -1,
            MaxSaturation = -1,
            CurrentGameMode = EnumGameMode.Guest,
            FreeMove = false,
            NoClip = false,
            PlayerUid = playerUid,
            PlayerName = player?.LastKnownPlayername ?? "unknown",
            Position = _nullBlockPos,
            MoveSpeedMultiplier = -1,
            Privileges = new []{"none"},
            Class = "unknown",
            RespawnUses = -1,
            ExtraLandClaimAllowance = player?.ExtraLandClaimAllowance ?? 0,
            ExtraLandClaimAreas = player?.ExtraLandClaimAreas ?? 0,
            Role = player?.RoleCode ?? "unknown",
            LandClaims = _sapi.WorldManager.SaveGame.LandClaims.Where(l => l.OwnedByPlayerUid != null && l.OwnedByPlayerUid.Equals(playerUid)).ToList(),
            Players = players
        };
        _serverNetworkChannel.SendPacket(data, fromPlayer);

        if (_receivedServerData.Contains(fromPlayer.PlayerUID)) return;
        
        _receivedServerData.Add(fromPlayer.PlayerUID);
        var rifteyeServerData = new RifteyeServerData
        {
            Roles = _sapi.Server.Config.Roles.Select(r => r.Code).ToArray()
        };
        _serverNetworkChannel.SendPacket(rifteyeServerData, fromPlayer);
    }

    private Dictionary<string, string> GetPlayers()
    {
        var players = new Dictionary<string,string>();
        foreach (var p in _sapi.PlayerData.PlayerDataByUid)
        {
            var isOnline = _sapi.World.AllOnlinePlayers.Any(po => po.PlayerUID.Equals(p.Value.PlayerUID));
            players.Add(p.Value.PlayerUID, isOnline ? p.Value.LastKnownPlayername : p.Value.LastKnownPlayername + " (Offline)");    
        }

        players = players.OrderBy(pair => (pair.Value.EndsWith("Offline)"), pair.Value)).ToDictionary(p => p.Key, p => p.Value);
        return players;
    }

    private IServerPlayer? LoadOfflinePlayer(string playerUid)
    {
        if (_offlinePlayerData.TryGetValue(playerUid, out var offlinePlayer))
        {
            return offlinePlayer;
        }
        
        try
        {
            var server = (ServerMain)_sapi.World;
            var chunkThread =
                typeof(ServerMain).GetField("chunkThread", BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(server)
                    as ChunkServerThread;
            var gameDatabase =
                typeof(ChunkServerThread).GetField("gameDatabase", BindingFlags.Instance | BindingFlags.NonPublic)
                    ?.GetValue(chunkThread) as GameDatabase;
            var playerData = gameDatabase?.GetPlayerData(playerUid);
            if (playerData == null || playerData.Length == 0)
            {
                return null;
            }
            ServerWorldPlayerData? playerWorldData;
            playerWorldData = SerializerUtil.Deserialize<ServerWorldPlayerData>(playerData);
            playerWorldData.Init(server);
            var serverPlayer = new ServerPlayer(server, playerWorldData);
            var initMethod = typeof(ServerPlayer).GetMethod("Init", BindingFlags.Instance | BindingFlags.NonPublic);
            initMethod?.Invoke(serverPlayer, null);
            if (serverPlayer.InventoryManager.GetOwnInventory(GlobalConstants.backpackInvClassName) is
                InventoryPlayerBackPacks bp)
            {
                var reloadMethod = typeof(InventoryPlayerBackPacks).GetMethod("ReloadBackPackSlots",
                    BindingFlags.Instance | BindingFlags.NonPublic);
                reloadMethod?.Invoke(bp, null);
            }

            _offlinePlayerData.TryAdd(playerUid, serverPlayer);

            return serverPlayer;    
        }
        catch (Exception e)
        {
            Mod.Logger.Error($"Failed loading offline player data for {playerUid}");
            Mod.Logger.Error(e);
            return null;
        }
    }

    private static RifteyeItemStackData[]? ToPacket(IInventory? inventory)
    {
        if (inventory == null)
        {
            return null;
        }
        var itemStacks = new RifteyeItemStackData[inventory.Count];
        for (var i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].Itemstack != null)
            {
                var ms = new MemoryStream();
                var writer = new BinaryWriter(ms);

                inventory[i].Itemstack.Attributes.ToBytes(writer);

                itemStacks[i] = new RifteyeItemStackData
                {
                    ItemClass = (int)inventory[i].Itemstack.Class,
                    ItemId = inventory[i].Itemstack.Id,
                    StackSize = inventory[i].Itemstack.StackSize,
                    Attributes = ms.ToArray()
                };
            }
            else
            {
                itemStacks[i] = new RifteyeItemStackData
                {
                    ItemClass = -1,
                    ItemId = 0,
                    StackSize = 0
                };
            }

        }
        return itemStacks;
    }
    public static ItemStack? FromPacket(RifteyeItemStackData fromPacket, IWorldAccessor resolver)
    {
        var attributes = new TreeAttribute();
        if (fromPacket.Attributes != null)
        {
            var ms = new MemoryStream(fromPacket.Attributes);
            var reader = new BinaryReader(ms);
            attributes.FromBytes(reader);
        }

        if(fromPacket.ItemClass == -1)
        {
            return null;
        }

        return new ItemStack(
            fromPacket.ItemId,
            (EnumItemClass)fromPacket.ItemClass,
            fromPacket.StackSize,
            attributes,
            resolver
        );
    }
}