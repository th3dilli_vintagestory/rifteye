using ProtoBuf;

namespace Rifteye.Data;

[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
internal class RifteyeInventoryData
{
    public RifteyeItemStackData[]? HotBar { get; set; }
    public RifteyeItemStackData[]? Backpack { get; set; }
    public RifteyeItemStackData[]? Crafting { get; set; }
    public RifteyeItemStackData[]? Character { get; set; }
    public RifteyeItemStackData[]? Mouse { get; set; }
}