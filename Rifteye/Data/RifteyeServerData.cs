using ProtoBuf;

namespace Rifteye.Data;

[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
internal class RifteyeServerData
{
    public string[]? Roles { get; set; }
}